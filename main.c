#include <math.h>
#include "raylib.h"

#define COLS 30
#define ROWS 20
#define TILE 20
#define BASESPEED 10.0f
#define INCSPEED  BASESPEED * 0.025f

#define TEXW 32
#define BUFSIZE COLS*ROWS

// ------------------------------------------------------------------------------

#define SPRITEMAX 5

Texture tHeads [SPRITEMAX]; 
Texture tBodies [SPRITEMAX]; 
Texture tTails [SPRITEMAX]; 
Texture tFruits [SPRITEMAX]; 
Texture tReady;
Texture tGameOver;

void loadTextures(){
	for( int i=0; i<SPRITEMAX; ++i){
		tHeads[i] = LoadTexture( TextFormat( "sprites/head/frame_00%i.png", i ));
		tBodies[i] = LoadTexture( TextFormat( "sprites/body/frame_00%i.png", i ));
		tTails[i] = LoadTexture( TextFormat( "sprites/tail/frame_00%i.png", i ));
		tFruits[i] = LoadTexture( TextFormat( "sprites/fruits/frame_00%i.png", i ));
	}
	tReady = LoadTexture( TextFormat( "sprites/ready.png" ));
	tGameOver = LoadTexture( TextFormat( "sprites/game_over.png" ));
}

void unloadTextures(){
	for( int i=0; i<SPRITEMAX; ++i){
		UnloadTexture( tHeads[i] );
		UnloadTexture( tBodies[i] );
		UnloadTexture( tTails[i] );
		UnloadTexture( tFruits[i] );
	}
	UnloadTexture( tReady );
	UnloadTexture( tGameOver );
}

// ------------------------------------------------------------------------------

#define GAME_READY 1
#define GAME_RUN 2
#define GAME_OVER 3

typedef struct Sect {
	int x;
	int y;
	int i;
	int ori;
} Sect;

Sect body[ BUFSIZE ]; // uninitialized, doesn't matter
Sect dir;
Sect fruit;
int head, tail;
int points;
int gameState;
float step, speed;

void newFruit(void){
	// it is possible for the fruit to drop on the snake body, but not the head  
	fruit = (Sect){ .x=GetRandomValue(0, ROWS-1), .y=GetRandomValue(0, COLS-1) };
	if ( fruit.x == body[head].x && fruit.y == body[head].y ){
		newFruit();
	}
	fruit.i = GetRandomValue(0, SPRITEMAX-1);
}

bool checkDeath(){
	int i=tail;

	while( i != head){
		if( body[head].x == body[i].x && body[head].y == body[i].y ){
			return true;
		}
		i++;
		if( i>=BUFSIZE){
			i=0;
		}
	}
	return false;
}

void setup(void){
	dir = (Sect) { .x=1, .y=0 };
	head = 1;
	tail = 0;
	body[head] = (Sect){ .x=5, .y=5, .ori=0 };
	body[tail] = (Sect){ .x=body[head].x-1, .y=body[head].y, .ori=0 };
	newFruit(); 
	points = 0;
	step = 0.0f;
	speed = BASESPEED;
}

void update(void){
	if (IsKeyPressed(KEY_RIGHT) && dir.x !=-1) dir = (Sect) { .x=1, .y=0, .ori=0 };
	if (IsKeyPressed(KEY_LEFT) && dir.x != 1) dir = (Sect) { .x=-1, .y=0, .ori=2 };
	if (IsKeyPressed(KEY_UP) && dir.y != 1) dir = (Sect) { .x=0, .y=-1, .ori=3 };
	if (IsKeyPressed(KEY_DOWN) && dir.y != -1) dir = (Sect) { .x=0, .y=1, .ori=1 };

	if( gameState == GAME_RUN ){
		step += GetFrameTime() * speed;
	
		if( step >= 1.0f ){
			step = 0.0f;
			
			int next = head+1;
			if( next >= BUFSIZE ){
				next = 0;
			}
			body[next].x = body[head].x + dir.x;
			body[next].y = body[head].y + dir.y;
			body[next].ori = dir.ori;
			body[next].i = GetRandomValue( 0, SPRITEMAX-1 );
			
			if( body[next].x < 0 ){
				body[next].x = ROWS-1;
			}
			if( body[next].x >= ROWS ){
				body[next].x = 0;
			}
			if( body[next].y < 0 ){
				body[next].y = COLS-1;
			}
			if( body[next].y >= COLS ){
				body[next].y = 0;
			}

			head = next;

			if( checkDeath() ){
				gameState = GAME_OVER;
			}else{
				if( body[head].x == fruit.x && body[head].y == fruit.y ){
					points++;
					speed = BASESPEED + INCSPEED * (float)points;
					newFruit();
				}else{
					tail++;
					if( tail >= BUFSIZE ){
						tail = 0;
					}
				}
			}
		}
	}else{
		if (IsKeyPressed(KEY_RIGHT) || IsKeyPressed(KEY_LEFT) 
			|| IsKeyPressed(KEY_UP) || IsKeyPressed(KEY_DOWN)) {
			if( gameState == GAME_OVER ){
				setup();
			}
			gameState = GAME_RUN;
		}
	}	
}


void drawTilted( Texture * tex, float x, float y, int ori ){
	Vector2 origin = (Vector2) {0.0f, 0.0f};
	switch( ori ){
		case 0:
    		DrawTexture(*tex, x, y, BLACK);
    	break;
		case 1:
            DrawTexturePro(*tex, 
            				(Rectangle){ 0.0f, 0.0f, 64.0f, 32.0f }, 
            				(Rectangle){ x + 32.0f, y, 64.0f, 32.0f }, 
            				origin, 90.0f, BLACK);
		break;		
		case 2:
            DrawTexturePro(*tex, 
            				(Rectangle){ 0.0f, 0.0f, 64.0f, 32.0f }, 
            				(Rectangle){ x + 32.0f, 
            				y + 32.0f, 64.0f, 32.0f }, 
            				origin, 180.0f, BLACK);
		break;
		case 3:
            DrawTexturePro(*tex, 
            				(Rectangle){ 0.0f, 0.0f, 64.0f, 32.0f }, 
            				(Rectangle){ x, y + 32.0f, 64.0f, 32.0f }, 
            				origin, 270.0f, BLACK);
		break;	
	}


} 

void draw(void){
    ClearBackground(WHITE);
    
	int i=tail;
	
	// draw tail
	//DrawRectangle( body[i].x * TILE, body[i].y * TILE, TILE, TILE, BLACK );

	int ori = body[i].ori;
	ori += 2;
	if( ori > 3 ) { ori -=4; }
	drawTilted( &tTails[body[i].i], body[i].x * TILE, body[i].y * TILE, ori );

    i++;
	while( i != head){
		// draw sec
		//DrawRectangle( body[i].x * TILE, body[i].y * TILE, TILE, TILE, BLACK );		
        DrawTexture(tBodies[body[i].i], body[i].x * TILE, body[i].y * TILE, BLACK);
		i++;
		if( i>=BUFSIZE){
			i=0;
		}
	}
	
	// draw head
	//DrawRectangle( body[i].x * TILE, body[i].y * TILE, TILE, TILE, BLACK );
	drawTilted( &tHeads[body[i].i], body[i].x * TILE, body[i].y * TILE, body[i].ori );

	// draw fruit
	//DrawRectangle( fruit.x * TILE, fruit.y * TILE, TILE, TILE, RED );
    DrawTexture(tFruits[fruit.i], fruit.x * TILE, fruit.y * TILE, RED);
}

int main() {
    int screenWidth = (ROWS-1) * TILE + TEXW;
    int screenHeight = (COLS-1) * TILE + TEXW;

	SetConfigFlags( FLAG_VSYNC_HINT );
    InitWindow(screenWidth, screenHeight, "snake");

	loadTextures();

	gameState = GAME_READY;
	setup();

	SetWindowPosition(0, 0);

	
    while (!WindowShouldClose()) {    
		update();
        BeginDrawing();  	

			draw();

			switch(gameState){
				case GAME_READY :
					DrawTexture( tReady, 0, 0, BLACK );
            		//DrawText("game ready, arrows to start", 10, 40, 20, RAYWHITE );
				break;				
				case GAME_RUN : default:
            		//DrawText(TextFormat("fruits: %i", points), 10, 40, 20, RAYWHITE );
				break;				

				case GAME_OVER :
					DrawTexture( tGameOver, 0, 0, BLACK );
            		//DrawText(TextFormat("fruits: %i - GAMEOVER, arrows to restart", points), 10, 40, 20, RAYWHITE );
				break;
				
			}
        EndDrawing();
    }

    
	unloadTextures();
    CloseWindow();
    return 0;
}